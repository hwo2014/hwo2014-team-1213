package noobbot;

import java.io.IOException;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import static com.google.common.collect.Lists.newArrayList;

public class TestApplication {
    // balancer
    public static final String SERVER_B = "testserver.helloworldopen.com";

    public static final String SERVER_R2 = "hakkinen.helloworldopen.com";
    public static final String SERVER_R1 = "senna.helloworldopen.com";
    public static final String SERVER_R3 = "webber.helloworldopen.com";


    public static final String PORT = "8091";
    public static final String TEAMKEY = "nh4DTfiA39gKQw";
    public static final ExecutorService EXECUTOR_SERVICE = Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors());

    public static final List<String> TRACKS = newArrayList("usa", "germany", "france", "finland");

    public static void main(final String[] args) throws Exception {
        if (args.length > 4) {
            for (int i = 0; i < Constants.THREAD_COUNTS; i++) {
                EXECUTOR_SERVICE.submit(new Callable<Object>() {
                    @Override
                    public Object call() throws Exception {
                        Main.main(args);
                        return null;
                    }
                });
            }
        }
        for (final String track : TRACKS) {
            for (int i = 0; i < Constants.THREAD_COUNTS; i++) {
                EXECUTOR_SERVICE.submit(new Callable<Object>() {
                    @Override
                    public Object call() throws Exception {
                        Main.main(args[0], args[1], args[2], args[3], track, "1", "pwd");
                        return null;
                    }
                });
            }
        }
        EXECUTOR_SERVICE.shutdown();
    }

    private static void runMultiple(final String botname, final String track, final String cars) throws IOException {
        int count = Integer.valueOf(cars);
        final String password = UUID.randomUUID().toString();

        for (int i=1; i<=count;i++) {
            final String newbot = botname+i;
            new Thread(new Runnable() {
                public void run() {
                    try {
                        Thread.sleep((long)(Math.random() * 1000));
                        Main.main(SERVER_R3, PORT, newbot, TEAMKEY, track, cars, password);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }).start();
        }
    }

    private static void runSingle(String botname) throws IOException {
        Main.main(SERVER_B, PORT, botname, TEAMKEY);
    }
}
