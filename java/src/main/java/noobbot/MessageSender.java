package noobbot;

import noobbot.messages.SendMsg;

public interface MessageSender {
    <T> void send(SendMsg<T> msg);
}
