package noobbot;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;
import com.google.gson.Gson;
import noobbot.messages.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.net.Socket;

import static com.google.common.base.Charsets.UTF_8;

public class Main {

    private final MessageSender messageSender;
    private final Gson gson = new Gson();

    private final BufferedReader messageReader;

    private static final Logger log = LoggerFactory.getLogger(Main.class);

    private final GameInfo gameInfo = new GameInfo();

    private final CarStrategy strategy;

    private final static Multimap<String, Double> bestResults = ArrayListMultimap.create();


    public Main(final BufferedReader reader, final PrintWriter writer) throws IOException {
        messageSender = new SimpleMessageSender(gson, writer);
        messageReader = reader;
        strategy = new SimpleCarStrategy(gameInfo, messageSender);
    }

    public static void main(String... args) throws IOException {

        log.info("Available processors is {}", Runtime.getRuntime().availableProcessors());

        String host = args[0];
        int port = Integer.parseInt(args[1]);
        String botName = args[2];
        String botKey = args[3];

        String track = null;
        String cars = null;
        String password = null;
        if (args.length > 4) {
            track = args[4];
            cars = args[5];
            password = args[6];
        }

        log.trace("Connecting to {}:{} as {}/{}", host, port, botName, botKey);

        final Socket socket = new Socket(host, port);
        final PrintWriter writer = new PrintWriter(new OutputStreamWriter(socket.getOutputStream(), UTF_8));
        final BufferedReader reader = new BufferedReader(new InputStreamReader(socket.getInputStream(), UTF_8));
        if (track == null) {
            new Main(reader, writer).join(botName, botKey);
        } else {
            new Main(reader, writer).join(botName, botKey, track, cars, password);
        }

        log.trace("RESULTS IS {}", bestResults);
    }

    private void join(String botName, String botKey) throws IOException {
        messageSender.send(new Join(botName, botKey));
        String line;
        while ((line = messageReader.readLine()) != null) {
            log.trace("IN: {}", line);
            onMessageFromServer(line);
        }
        bestResults.put(gameInfo.getTrack(), gameInfo.getBestTime());
    }

    private void join(String botName, String botKey, String track, String cars, String pass) throws IOException {
        messageSender.send(new JoinRace(botName, botKey, track, cars, pass));

        String line;
        while ((line = messageReader.readLine()) != null) {
            log.trace("IN: {}", line);
            onMessageFromServer(line);
        }
        bestResults.put(gameInfo.getTrack(), gameInfo.getBestTime());
    }


    private void onMessageFromServer(String rawMessage) {
        TypedWrapper typed = gson.fromJson(rawMessage, TypedWrapper.class);
        if (typed.msgType.equals("carPositions")) {
            CarPositionMessage message = gson.fromJson(rawMessage, CarPositionMessage.class);
            log.trace("on carPositions: {}", message);
            gameInfo.updateInfo(message.data);
            strategy.onCarPosition();
        } else if (typed.msgType.equals("yourCar")) {
            YourCartMessage message = gson.fromJson(rawMessage, YourCartMessage.class);
            log.trace("on yourCar: {}", message);
            gameInfo.setYourCar(message.data);
            messageSender.send(new Ping());
        } else if (typed.msgType.equals("join")) {
            MsgWrapper message = gson.fromJson(rawMessage, MsgWrapper.class);
            log.trace("on event: {}", message);
        } else if (typed.msgType.equals("gameInit")) {
            GameInitMessage message = gson.fromJson(rawMessage, GameInitMessage.class);
            log.trace("on gameInit: {}", message);
            gameInfo.updateInfo(message.data.race);
        } else if (typed.msgType.equals("gameEnd")) {
            GameEndMessage message = gson.fromJson(rawMessage, GameEndMessage.class);
            log.trace("on gameEnd: {}", message);
            gameInfo.updateResult(message.data.bestLaps);
        } else if (typed.msgType.equals("gameStart")) {
            GameStartMessage message = gson.fromJson(rawMessage, GameStartMessage.class);
            log.trace("on gameStart: {}", message);
        } else {
            //TODO looking into existing messages
            MsgWrapper message = gson.fromJson(rawMessage, MsgWrapper.class);
            log.trace("on event: {}", message);
        }
        messageSender.send(new Ping());
    }

}
