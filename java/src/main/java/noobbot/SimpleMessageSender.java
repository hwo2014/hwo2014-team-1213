package noobbot;

import com.google.gson.Gson;
import noobbot.messages.MsgWrapper;
import noobbot.messages.SendMsg;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.PrintWriter;

public class SimpleMessageSender implements MessageSender {

    private static final Logger log = LoggerFactory.getLogger(SimpleMessageSender.class);
    private final Gson gson;
    private final PrintWriter writer;

    public SimpleMessageSender(Gson gson, PrintWriter writer) {
        this.gson = gson;
        this.writer = writer;
    }


    @Override
    public <T> void send(final SendMsg<T> msg) {
        String jsonMessage = gson.toJson(new MsgWrapper<T>(msg));
        log.trace("OUT: {}", jsonMessage);
        writer.println(jsonMessage);
        writer.flush();
    }
}
