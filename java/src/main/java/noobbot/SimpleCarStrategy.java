package noobbot;

import noobbot.messages.Throttle;
import noobbot.messages.Turbo;
import noobbot.to.CarPositionTO;
import noobbot.to.PieceTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static java.lang.Math.abs;
import static java.lang.String.format;

public class SimpleCarStrategy implements CarStrategy {
    private static final Logger log = LoggerFactory.getLogger(SimpleCarStrategy.class);
    private final GameInfo gameInfo;
    private final MessageSender messageSender;

    public SimpleCarStrategy(GameInfo gameInfo, MessageSender messageSender) {
        this.gameInfo = gameInfo;
        this.messageSender = messageSender;
    }

    @Override
    public void onCarPosition() {
        CarPositionTO currentCartPosition = gameInfo.getCurrentYourCartPosition();
        PieceTO currentPiece = gameInfo.getCurrentPiece();

        double value;
        double currentAngle = currentCartPosition.angle;
        double angle = currentPiece.angle;
        if (gameInfo.isLineOrEndOfAngle()) {
            double nextAngleFactor = gameInfo.getNextAngleFactor();
            double availableToNextAngle = gameInfo.getAvailableNiceLength();
            log.trace("available before angle = {}", availableToNextAngle);
            if (availableToNextAngle > 500 && gameInfo.isLastLap()) {
                messageSender.send(new Turbo());
            }
            if (availableToNextAngle > 400) {
                value = 1;
            } else if (availableToNextAngle > 300) {
                value = 0.95;
            } else if (availableToNextAngle > 200) {
                value = 0.8 * nextAngleFactor;
            } else if (availableToNextAngle > 100) {
                value = 0.75 * nextAngleFactor;
            } else {
                value = 0.7 * nextAngleFactor ;
            }
        } else {
            double angleFactor = 1 - abs(currentAngle) / Constants.DANGER_ANGEL;
            value = 0.7 * angleFactor;
        }

        value = value > 0 ? value : 0;
        log.trace("current angle is {}, cart angle is {}, speed is {}", format("%.4f", currentAngle), format("%.4f", angle), format("%.4f", value));
        messageSender.send(new Throttle(value));
    }
}
