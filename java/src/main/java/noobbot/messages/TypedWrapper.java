package noobbot.messages;

public class TypedWrapper {
    public final String msgType;

    public TypedWrapper(String msgType) {
        this.msgType = msgType;
    }

}
