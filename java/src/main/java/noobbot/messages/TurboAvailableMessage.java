package noobbot.messages;

import noobbot.to.TurboAvailableTO;

public class TurboAvailableMessage extends MsgWrapper<TurboAvailableTO> {
    public TurboAvailableMessage(String msgType, TurboAvailableTO data) {
        super(msgType, data);
    }
}
