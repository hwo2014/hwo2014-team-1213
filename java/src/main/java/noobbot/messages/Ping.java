package noobbot.messages;

public class Ping extends SendMsg<Object> {
    @Override
    protected String msgType() {
        return "ping";
    }
}