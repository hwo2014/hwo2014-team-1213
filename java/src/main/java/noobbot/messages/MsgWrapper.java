package noobbot.messages;

import com.google.common.base.Objects;

public class MsgWrapper<T> extends TypedWrapper{
    public final T data;

    public MsgWrapper(String msgType, T data) {
        super(msgType);
        this.data = data;
    }

    public MsgWrapper(SendMsg<T> sendMsg) {
        //noinspection unchecked
        this(sendMsg.msgType(), (T) sendMsg.msgData());
    }

    @Override
    public String toString() {
        return Objects.toStringHelper(this)
                .add("msgType", msgType)
                .add("data", data)
                .toString();
    }
}