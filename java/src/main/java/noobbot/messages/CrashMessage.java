package noobbot.messages;

import noobbot.to.CarIdTO;

public class CrashMessage extends MsgWrapper<CarIdTO>{
    public CrashMessage(String msgType, CarIdTO data) {
        super(msgType, data);
    }
}
