package noobbot.messages;

import noobbot.to.CarPositionTO;

import java.util.List;

public class CarPositionMessage extends MsgWrapper<List<CarPositionTO>>{

    public final String gameId;
    public CarPositionMessage(String msgType, List<CarPositionTO> data, String gameId) {
        super(msgType, data);
        this.gameId = gameId;
    }
}
