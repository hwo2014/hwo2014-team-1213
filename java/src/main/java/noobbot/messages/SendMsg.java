package noobbot.messages;

public abstract class SendMsg<T> {

    protected Object msgData() {
        return this;
    }

    protected abstract String msgType();
}