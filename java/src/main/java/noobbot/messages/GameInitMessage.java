package noobbot.messages;

import noobbot.to.GameTO;

public class GameInitMessage extends MsgWrapper<GameTO>{
    public GameInitMessage(String msgType, GameTO data) {
        super(msgType, data);
    }
}
