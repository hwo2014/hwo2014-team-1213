package noobbot.messages;

import noobbot.to.GameEndTO;

public class GameEndMessage extends MsgWrapper<GameEndTO>{
    public GameEndMessage(String msgType, GameEndTO data) {
        super(msgType, data);
    }
}

