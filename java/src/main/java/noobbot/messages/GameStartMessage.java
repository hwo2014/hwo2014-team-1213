package noobbot.messages;

import com.google.common.base.Objects;

public class GameStartMessage extends MsgWrapper<Object>{

    public final String gameId;
    public final int gameTick;

    public GameStartMessage(String msgType, Object data, String gameId, int gameTick) {
        super(msgType, data);
        this.gameId = gameId;
        this.gameTick = gameTick;
    }


    @Override
    public String toString() {
        return Objects.toStringHelper(this)
                .add("gameId", gameId)
                .add("gameTick", gameTick)
                .toString();
    }
}
