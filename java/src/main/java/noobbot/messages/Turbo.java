package noobbot.messages;

public class Turbo extends SendMsg<String> {
    private double value;

    @Override
    protected Object msgData() {
        return "Throttle";
    }

    @Override
    protected String msgType() {
        return "turbo";
    }
}