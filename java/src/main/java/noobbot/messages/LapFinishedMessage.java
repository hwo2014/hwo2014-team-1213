package noobbot.messages;

import noobbot.to.LapFinishedTO;

public class LapFinishedMessage extends MsgWrapper<LapFinishedTO>{
    public LapFinishedMessage(String msgType, LapFinishedTO data) {
        super(msgType, data);
    }
}
