package noobbot.messages;

import noobbot.to.CarIdTO;

public class YourCartMessage extends MsgWrapper<CarIdTO> {
    public final String gameId;
    public YourCartMessage(String msgType, CarIdTO data, String gameId) {
        super(msgType, data);
        this.gameId = gameId;
    }
}
