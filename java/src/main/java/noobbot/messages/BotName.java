package noobbot.messages;

/**
 * Created by pavel on 20/04/14.
 */
public class BotName {
    public final String name;
    public final String key;

    public BotName(String name, String key) {
        this.name = name;
        this.key = key;
    }
}
