package noobbot.messages;

import noobbot.to.CarIdTO;

public class FinishMessage extends MsgWrapper<CarIdTO> {
    public FinishMessage(String msgType, CarIdTO data) {
        super(msgType, data);
    }
}