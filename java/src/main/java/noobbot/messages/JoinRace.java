package noobbot.messages;

public class JoinRace extends SendMsg {
    public final BotName botId;
    public final String carCount;
    public final String trackName;
    public final String password;

    public JoinRace(final String name, final String key, String track, String cars, String pass) {
        botId = new BotName(name, key);
        carCount = cars;
        trackName = track;
        password = pass;
    }

    @Override
    protected String msgType() {
        return "joinRace";
    }
}
