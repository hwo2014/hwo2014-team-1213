package noobbot;

public interface Constants {
    double SAFETY_ANGEL = 30;
    double DANGER_ANGEL = 50;
    double END_OF_PIECE_RATE = 0.7;
    int THREAD_COUNTS = 1;
}
