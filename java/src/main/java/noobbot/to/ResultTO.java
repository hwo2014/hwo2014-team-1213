package noobbot.to;

import com.google.common.base.Objects;

public class ResultTO {
    public final CarIdTO car;
    public final CarResultTO result;


    public ResultTO(CarIdTO car, CarResultTO result) {
        this.car = car;
        this.result = result;
    }

    @Override
    public String toString() {
        return Objects.toStringHelper(this)
                .add("car", car)
                .add("result", result)
                .toString();
    }
}
