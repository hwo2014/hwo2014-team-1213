package noobbot.to;

import com.google.common.base.Objects;

import static java.lang.String.format;

public class RaceSessionTO {
    public final int laps;
    public final double maxLapTimeMs;
    public final boolean quickRace;

    public RaceSessionTO(int laps, double maxLapTimeMs, boolean quickRace) {
        this.laps = laps;
        this.maxLapTimeMs = maxLapTimeMs;
        this.quickRace = quickRace;
    }

    @Override
    public String toString() {
        return Objects.toStringHelper(this)
                .add("laps", laps)
                .add("maxLapTimeMs",format("%.4f", maxLapTimeMs))
                .add("quickRace", quickRace)
                .toString();
    }
}
