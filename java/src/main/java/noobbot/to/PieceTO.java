package noobbot.to;

import com.google.common.base.Objects;
import com.google.gson.annotations.SerializedName;
import noobbot.Constants;

import static java.lang.String.format;

public class PieceTO {
    public final double length;
    @SerializedName("switch")
    public final boolean haveSwitch;
    public final double radius;
    public final double angle;

    public PieceTO(double length, boolean haveSwitch, double radius, double angle) {
        this.length = length;
        this.haveSwitch = haveSwitch;
        this.radius = radius;
        this.angle = angle;
    }

    public double getAngleFactor() {
        if (isHaveAngle()){
            double factor = Constants.SAFETY_ANGEL / Math.abs(angle);
            return factor > 1 ? 1 : factor;
        } else {
            return 1;
        }
    }

    public double getLength(double centerDiff) {
        if (length > 0) {
            return length;
        } else if (isHaveAngle()) {
            return Math.PI * (radius - Math.signum(angle) * centerDiff) * Math.abs(angle) / 180;
        } else {
            throw new IllegalArgumentException("wrong piece " + this);
        }
    }

    public boolean isHaveAngle(){
        return Math.abs(angle) > 0 && Math.abs(radius) > 0;
    }

    @Override
    public String toString() {
        return Objects.toStringHelper(this)
                .add("length", format("%.4f", length))
                .add("haveSwitch", haveSwitch)
                .add("radius", format("%.4f", radius))
                .add("angle", format("%.4f", angle))
                .toString();
    }
}
