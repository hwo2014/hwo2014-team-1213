package noobbot.to;

import com.google.common.base.Objects;

import java.util.List;

public class GameEndTO {

    public final List<ResultTO> results;
    public final List<ResultTO> bestLaps;

    public GameEndTO(List<ResultTO> results, List<ResultTO> bestLaps) {
        this.results = results;
        this.bestLaps = bestLaps;
    }

    @Override
    public String toString() {
        return Objects.toStringHelper(this)
                .add("results", results)
                .add("bestLaps", bestLaps)
                .toString();
    }
}
