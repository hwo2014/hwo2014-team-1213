package noobbot.to;

import com.google.common.base.Objects;

public class RankingTO {
    public final int overall;
    public final int fastestLap;

    public RankingTO(int overall, int fastestLap) {
        this.overall = overall;
        this.fastestLap = fastestLap;
    }

    @Override
    public String toString() {
        return Objects.toStringHelper(this)
                .add("overall", overall)
                .add("fastestLap", fastestLap)
                .toString();
    }
}
