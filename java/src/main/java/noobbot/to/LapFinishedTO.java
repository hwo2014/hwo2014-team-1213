package noobbot.to;

import com.google.common.base.Objects;

public class LapFinishedTO {
    public final CarIdTO car;
    public final CarResultTO raceTime;
    public final LapTimeTO lapTime;
    public final RankingTO ranking;

    public LapFinishedTO(CarIdTO car, CarResultTO raceTime, LapTimeTO lapTime, RankingTO ranking) {
        this.car = car;
        this.raceTime = raceTime;
        this.lapTime = lapTime;
        this.ranking = ranking;
    }

    @Override
    public String toString() {
        return Objects.toStringHelper(this)
                .add("car", car)
                .add("raceTime", raceTime)
                .add("lapTime", lapTime)
                .add("ranking", ranking)
                .toString();
    }
}
