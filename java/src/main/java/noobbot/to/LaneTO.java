package noobbot.to;

import com.google.common.base.Objects;

import static java.lang.String.format;

public class LaneTO {
    public final double distanceFromCenter;
    public final int index;

    public LaneTO(double distanceFromCenter, int index) {
        this.distanceFromCenter = distanceFromCenter;
        this.index = index;
    }

    @Override
    public String toString() {
        return Objects.toStringHelper(this)
                .add("distanceFromCenter", format("%.4f", distanceFromCenter))
                .add("index", index)
                .toString();
    }
}
