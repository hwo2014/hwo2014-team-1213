package noobbot.to;

import com.google.common.base.Objects;

public class GameTO {
    public final RaceTO race;

    public GameTO(RaceTO race) {
        this.race = race;
    }

    @Override
    public String toString() {
        return Objects.toStringHelper(this)
                .add("race", race)
                .toString();
    }
}
