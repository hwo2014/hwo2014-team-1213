package noobbot.to;

import com.google.common.base.Objects;

import java.util.List;

public class RaceTO {
    public final List<CarTO> cars;
    public final RaceSessionTO raceSession;
    public final TrackTO track;

    public RaceTO(List<CarTO> cars, RaceSessionTO raceSession, TrackTO track) {
        this.cars = cars;
        this.raceSession = raceSession;
        this.track = track;
    }

    @Override
    public String toString() {
        return Objects.toStringHelper(this)
                .add("cars", cars)
                .add("raceSession", raceSession)
                .add("track", track)
                .toString();
    }

}
