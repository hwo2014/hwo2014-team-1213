package noobbot.to;

import com.google.common.base.Objects;

public class CarResultTO {
    public final int laps;
    public final int ticks;
    public final int millis;

    public CarResultTO(int laps, int ticks, int millis) {
        this.laps = laps;
        this.ticks = ticks;
        this.millis = millis;
    }

    @Override
    public String toString() {
        return Objects.toStringHelper(this)
                .add("laps", laps)
                .add("ticks", ticks)
                .add("millis", millis)
                .toString();
    }
}
