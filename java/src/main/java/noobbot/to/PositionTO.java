package noobbot.to;

import com.google.common.base.Objects;

import static java.lang.String.format;

public class PositionTO {
    public final double x;
    public final double y;

    public PositionTO(double x, double y) {
        this.x = x;
        this.y = y;
    }

    @Override
    public String toString() {
        return Objects.toStringHelper(this)
                .add("x", format("%.4f", x))
                .add("y", format("%.4f", y))
                .toString();
    }
}
