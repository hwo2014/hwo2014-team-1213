package noobbot.to;

import com.google.common.base.Objects;

import static java.lang.String.format;

public class TurboAvailableTO {
   public final int turboDurationMilliseconds;
    public final int turboDurationTicks;
    public final double turboFactor;


    public TurboAvailableTO(int turboDurationMilliseconds, int turboDurationTicks, double turboFactor) {
        this.turboDurationMilliseconds = turboDurationMilliseconds;
        this.turboDurationTicks = turboDurationTicks;
        this.turboFactor = turboFactor;
    }

    @Override
    public String toString() {
        return Objects.toStringHelper(this)
                .add("turboDurationMilliseconds", turboDurationMilliseconds)
                .add("turboDurationTicks", turboDurationTicks)
                .add("turboFactor", format("%.4f", turboFactor))
                .toString();
    }
}
