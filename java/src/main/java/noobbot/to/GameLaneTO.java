package noobbot.to;

import com.google.common.base.Objects;

public class GameLaneTO {
    public final int startLaneIndex;
    public final int endLaneIndex;

    public GameLaneTO(int startLaneIndex, int endLaneIndex) {
        this.startLaneIndex = startLaneIndex;
        this.endLaneIndex = endLaneIndex;
    }

    @Override
    public String toString() {
        return Objects.toStringHelper(this)
                .add("startLaneIndex", startLaneIndex)
                .add("endLaneIndex", endLaneIndex)
                .toString();
    }
}
