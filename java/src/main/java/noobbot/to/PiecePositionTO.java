package noobbot.to;

import com.google.common.base.Objects;

import static java.lang.String.format;

public class PiecePositionTO {
    public final int pieceIndex;
    public final double inPieceDistance;
    public final GameLaneTO lane;
    public final int lap;

    public PiecePositionTO(int pieceIndex, double inPieceDistance, GameLaneTO lane, int lap) {
        this.pieceIndex = pieceIndex;
        this.inPieceDistance = inPieceDistance;
        this.lane = lane;
        this.lap = lap;
    }

    @Override
    public String toString() {
        return Objects.toStringHelper(this)
                .add("pieceIndex", pieceIndex)
                .add("inPieceDistance", format("%.4f", inPieceDistance))
                .add("lane", lane)
                .add("lap", lap)
                .toString();
    }
}
