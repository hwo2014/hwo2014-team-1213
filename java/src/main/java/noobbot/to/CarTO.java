package noobbot.to;

import com.google.common.base.Objects;

public class CarTO {
    public final CarIdTO id;
    public final DimenisionTO dimensions;

    public CarTO(CarIdTO id, DimenisionTO dimensions) {
        this.id = id;
        this.dimensions = dimensions;
    }

    @Override
    public String toString() {
        return Objects.toStringHelper(this)
                .add("id", id)
                .add("dimensions", dimensions)
                .toString();
    }
}