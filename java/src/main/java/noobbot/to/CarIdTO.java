package noobbot.to;

import com.google.common.base.Objects;

public class CarIdTO {
    public final String name;
    public final String color;

    public CarIdTO(String name, String color) {
        this.name = name;
        this.color = color;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof CarIdTO)) {
            return false;
        }
        CarIdTO carIdTO = (CarIdTO) o;
        return color.equals(carIdTO.color) && name.equals(carIdTO.name);
    }

    @Override
    public int hashCode() {
        int result = name.hashCode();
        result = 31 * result + color.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return Objects.toStringHelper(this)
                .add("name", name)
                .add("color", color)
                .toString();
    }
}
