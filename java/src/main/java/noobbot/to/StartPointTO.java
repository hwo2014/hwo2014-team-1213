package noobbot.to;

import com.google.common.base.Objects;

import static java.lang.String.format;

public class StartPointTO {
    public final PositionTO position;
    public final double angle;

    public StartPointTO(PositionTO position, double angle) {
        this.position = position;
        this.angle = angle;
    }

    @Override
    public String toString() {
        return Objects.toStringHelper(this)
                .add("position", position)
                .add("angle", format("%.4f", angle))
                .toString();
    }
}
