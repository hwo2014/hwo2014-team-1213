package noobbot.to;

import com.google.common.base.Objects;

import static java.lang.String.format;

public class CarPositionTO {
    public final CarIdTO id;
    public final double angle;
    public final PiecePositionTO piecePosition;

    public CarPositionTO(CarIdTO id, double angle, PiecePositionTO piecePosition) {
        this.id = id;
        this.angle = angle;
        this.piecePosition = piecePosition;
    }

    @Override
    public String toString() {
        return Objects.toStringHelper(this)
                .add("id", id)
                .add("angle", format("%.4f", angle))
                .add("piecePosition", piecePosition)
                .toString();
    }
}
