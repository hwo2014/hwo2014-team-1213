package noobbot.to;

import com.google.common.base.Objects;

import java.util.List;

public class TrackTO {
    public final String id;
    public final String name;
    public final List<PieceTO> pieces;
    public final List<LaneTO> lanes;
    public final StartPointTO startingPoint;

    public TrackTO(String id, String name, List<PieceTO> pieces, List<LaneTO> lanes, StartPointTO startingPoint) {
        this.id = id;
        this.name = name;
        this.pieces = pieces;
        this.lanes = lanes;
        this.startingPoint = startingPoint;
    }

    @Override
    public String toString() {
        return Objects.toStringHelper(this)
                .add("id", id)
                .add("name", name)
                .add("pieces", pieces)
                .add("lanes", lanes)
                .add("startingPoint", startingPoint)
                .toString();
    }
}
