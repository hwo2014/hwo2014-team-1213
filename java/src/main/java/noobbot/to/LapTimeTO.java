package noobbot.to;

import com.google.common.base.Objects;

public class LapTimeTO {
    public final int lap;
    public final int ticks;
    public final int millis;

    public LapTimeTO(int lap, int ticks, int millis) {
        this.lap = lap;
        this.ticks = ticks;
        this.millis = millis;
    }


    @Override
    public String toString() {
        return Objects.toStringHelper(this)
                .add("lap", lap)
                .add("ticks", ticks)
                .add("millis", millis)
                .toString();
    }
}

