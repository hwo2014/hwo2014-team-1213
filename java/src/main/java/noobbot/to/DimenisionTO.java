package noobbot.to;

import com.google.common.base.Objects;

import static java.lang.String.format;

public class DimenisionTO {
    public final double length;
    public final double width;
    public final double guideFlagPosition;

    public DimenisionTO(double length, double width, double guideFlagPosition) {
        this.length = length;
        this.width = width;
        this.guideFlagPosition = guideFlagPosition;
    }

    @Override
    public String toString() {
        return Objects.toStringHelper(this)
                .add("length", format("%.4f", length))
                .add("width", format("%.4f", width))
                .add("guideFlagPosition", format("%.4f", guideFlagPosition))
                .toString();
    }
}