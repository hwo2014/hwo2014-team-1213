package noobbot;

import com.google.common.base.Function;
import com.google.common.base.Predicate;
import com.google.common.collect.FluentIterable;
import com.google.common.collect.Iterables;
import noobbot.to.*;

import java.util.List;

import static com.google.common.base.Predicates.compose;
import static com.google.common.base.Predicates.equalTo;

public class GameInfo {
    private CarIdTO yourCar;
    private List<PieceTO> pieces;
    private RaceSessionTO raceSession;
    private CarPositionTO currentYourCartPosition;
    private CarPositionTO previousYourCartPosition;
    private FluentIterable<PieceTO> currentPieces;
    private double availableNiceLenght;
    private PieceTO currentPiece;
    private PieceTO previousPiece;
    private double pieceDistance;
    private List<LaneTO> lines;
    private LaneTO currentLine;
    private String track;
    private double bestTime;


    public double getAvailableNiceLength() {
        double availableToNextAngle = currentPiece.getLength(currentLine.distanceFromCenter) - pieceDistance;
        for (int i = 1; i < 10; i++) {
            PieceTO pos = currentPieces.get(i);
            if (pos.isHaveAngle()){
                break;
            } else {
                availableToNextAngle += pos.getLength(currentLine.distanceFromCenter);
            }
        }
        return isLineOrEndOfAngle() ? availableToNextAngle : 0;
    }

    public double getBestTime() {
        return bestTime;
    }

    public String getTrack() {
        return track;
    }

    public void updateResult(List<ResultTO> bestLaps) {
        bestTime = FluentIterable.from(bestLaps).firstMatch(compose(equalTo(yourCar), resultToId())).transform(new Function<ResultTO, Double>() {
            @Override
            public Double apply(ResultTO input) {
                return input.result.millis / 1000d;
            }
        }).or(Double.MAX_VALUE);
    }

    private Function<ResultTO, CarIdTO> resultToId() {
        return new Function<ResultTO, CarIdTO>() {
            @Override
            public CarIdTO apply(ResultTO input) {
                return input.car;
            }
        };
    }

    public boolean isLineOrEndOfAngle() {
        double endOfPieceRate = pieceDistance / currentPiece.getLength(currentLine.distanceFromCenter);
        return !currentPiece.isHaveAngle() || endOfPieceRate > Constants.END_OF_PIECE_RATE;
    }

    public List<PieceTO> getPieces() {
        return pieces;
    }

    public RaceSessionTO getRaceSession() {
        return raceSession;
    }

    public CarIdTO getYourCar() {
        return yourCar;
    }


    public void setYourCar(CarIdTO yourCar) {
        this.yourCar = yourCar;
    }


    public void updateInfo(List<CarPositionTO> positions){
        previousYourCartPosition = currentYourCartPosition;
        currentYourCartPosition = getCarPositionTO(positions);
        int currentPiecesIndex = currentYourCartPosition.piecePosition.pieceIndex;
        currentPieces = FluentIterable.from(Iterables.cycle(pieces)).skip(currentPiecesIndex);
        previousPiece = currentPiece;
        currentPiece = currentPieces.get(0);
        pieceDistance = currentYourCartPosition.piecePosition.inPieceDistance;
        int lineStartIndex = currentYourCartPosition.piecePosition.lane.startLaneIndex;
        currentLine = FluentIterable.from(lines).firstMatch(getPredicate(lineStartIndex)).get();
    }

    private Predicate<LaneTO> getPredicate(final int lineIndex) {
        return new Predicate<LaneTO>() {
            @Override
            public boolean apply(LaneTO input) {
                return input.index == lineIndex;
            }
        };
    }

    public CarPositionTO getCurrentYourCartPosition() {
        return currentYourCartPosition;
    }

    public void updateInfo(RaceTO race) {
        pieces = race.track.pieces;
        lines = race.track.lanes;
        raceSession = race.raceSession;
        track = race.track.name;
    }

    private CarPositionTO getCarPositionTO(List<CarPositionTO> positions) {
        return FluentIterable.from(positions)
                .firstMatch(compose(equalTo(yourCar), cartPositionToId()))
                .get();
    }

    private Function<CarPositionTO, CarIdTO> cartPositionToId() {
        return new Function<CarPositionTO, CarIdTO>() {
            @Override
            public CarIdTO apply(CarPositionTO input) {
                return input.id;
            }
        };
    }

    public FluentIterable<PieceTO> getCurrentPieces() {
        return currentPieces;
    }

    public PieceTO getCurrentPiece() {
        return currentPiece;
    }

    public boolean isLastLap(){
        return currentYourCartPosition.piecePosition.lap + 1 == raceSession.laps;
    }

    public double getNextAngleFactor(){
        return currentPieces.skip(1)
                .firstMatch(new Predicate<PieceTO>() {
                    @Override
                    public boolean apply(PieceTO input) {
                        return input.isHaveAngle();
                    }
                })
                .transform(new Function<PieceTO, Double>() {
                    @Override
                    public Double apply(PieceTO input) {
                        return input.getAngleFactor();
                    }
                })
                .or(1d);
    }

}
